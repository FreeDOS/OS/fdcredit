# fdcredit

# FreeDOS Credits

FreeDOS LiveCD Credits

A simple program to display a thank you message to many of the contributors
that have and do make FreeDOS possible.

This program requires [the Danger Engine](https://gitlab.com/DangerApps/danger) to compile.
